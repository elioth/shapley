#shapley is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import numpy as np
import pandas as pd
import pomegranate as pog
import random
from deap import creator, base, tools, algorithms
import importlib
from plotly.offline import plot
import plotly.graph_objs as go
import networkx as nx

import random
from deap import base
from deap import creator
from deap import tools
from deap.algorithms import varAnd

from .utils import cartesian_product

class Shapley():

    def __init__(self):
        self.available_actions = []
        self.independent_actions = []
        self.dependent_actions = []
        self.value_model = None
        self.coalitions = None
        
    def add_value_model(self, model):
        """
            Stores the quantitative model used to compute the worth of a coalition.

            Args:
                model: a function taking a dict of action, value pairs and returns the worth of the corresponding coalition 
        """
        self.model = model
        
    def add_available_actions(self, actions):
        """
            Stores the available actions (variables) used in the model.

            Args:
                actions (list): a list of Action objects.
        """
        self.available_actions += actions

    def find_best_coalition(self, n_population, n_generations, n_no_improvement=None, available_values=None, minimize=True):

        # Store a reference to the global object
        # (used in the Individual class)
        shapley = self

        self.prepare_permission_structure(available_values)

        variable_index = {}
        for i in range(len(self.bayesian_network.states)):
            s = self.bayesian_network.states[i]
            variable_index[s.name] = i


        # Create an Individual class holding the actions of the corresponding coalition
        # Actions are initialized randomly based on the permission structure (encoded by the bayesian network)
        class Individual:
            def __init__(self):
                self.actions = shapley.sample_coalitions(1)[0].actions

        # The evaluation function of the genetic algorithm just wraps the model registered in the Shapley object
        def evaluate(ind):
            return self.model(ind.actions), 
        
        # Children are random combinations of their parents
        def mate(ind1, ind2):

            action_names = list(ind1.actions.keys())
            random.shuffle(action_names)
            parents = [toolbox.clone(ind1), toolbox.clone(ind2)]
            children_actions = [{}, {}]

            # For each variable, we choose the value of one parent or of the other with a 50/50 probability
            for i in [0, 1]:
                for name in action_names:

                    # Compute the probability of the action's values
                    proba = self.bayesian_network.predict_proba([children_actions[i]])[0]
                    v = list(proba[variable_index[name]].parameters[0].keys())
                    p = list(proba[variable_index[name]].parameters[0].values())

                    # Choose a parent at random
                    parent = parents[random.randint(0, 1)]
                    
                    # If the value of the parent is possible,
                    # set the value of the child to the value of the parent
                    if p[v.index(parent.actions[name])] > 0.0:
                        children_actions[i][name] = parent.actions[name]

                    # If not, sample a value among the possible values
                    else:
                        children_actions[i][name] = np.random.choice(v, 1, p=p).tolist()[0]

            # Create the children individuals
            children = [toolbox.clone(ind1), toolbox.clone(ind1)]
            children[0].actions = children_actions[0]
            children[1].actions = children_actions[1]

            return children

        # Individuals are mutated by removing the values of randomly chosen variables
        # And then resetting them by sampling the bayesian network
        def mutate(ind):

            # Clone the base individual to avoid modifying it in place
            mutated_ind = toolbox.clone(ind)

            # Choose which variables should be mutated, with a 50% probability
            fixed_variables = []
            mutated_variables = []
            for k, v in mutated_ind.actions.items():
                if random.random() < 0.5:
                    mutated_variables.append(k)
                else:
                    fixed_variables.append(k)

            # Shuffle the mutated variables to avoid sampling in the same order in every mutation
            random.shuffle(mutated_variables)

            # Sample a value for each mutated variable
            for k in mutated_variables:
                actions = {a: mutated_ind.actions[a] for a in fixed_variables if a in mutated_ind.actions.keys()}
                proba = self.bayesian_network.predict_proba([actions])[0]
                v = list(proba[variable_index[k]].parameters[0].keys())
                p = list(proba[variable_index[k]].parameters[0].values())
                mutated_ind.actions[k] = np.random.choice(v, 1, p=p).tolist()[0]
                fixed_variables.append(k)

            return mutated_ind,      

        # Initialize the fitness operator (minimization / maximization)
        # and the individual container based on the Individual class
        creator.create("FitnessMax", base.Fitness, weights=(minimize*-2+1,))
        creator.create("Individual", Individual, fitness=creator.FitnessMax)

        # Register all functions necessary for the genetic algorithm
        toolbox = base.Toolbox()
        toolbox.register("individual", creator.Individual)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)
        toolbox.register("evaluate", evaluate)
        toolbox.register("mate", mate)
        toolbox.register("mutate", mutate)
        toolbox.register("select", tools.selTournament, tournsize=3)

        # Prepare the container for the best solution
        hall_of_fame = tools.HallOfFame(1)

        # Generate the base population
        population = toolbox.population(n=n_population)

        # Initialize the early stopping parameters
        # (algorithm should stop if no improvement is achieved for n_no_improvement generations)
        k_no_improvement = 0
        if n_no_improvement is None:
            n_no_improvement = n_generations

        # Run the algorithm for n_gen generations
        for gen in range(n_generations):

            population = toolbox.select(population, len(population))
            
            # Generate the offspring based on a 50% mating probability and 25% mutation probability
            offspring = varAnd(population, toolbox, cxpb=0.5, mutpb=0.25)
        
            # Evaluate the fitness of each individual in the new population
            # and find the best one
            if minimize:
                best_fit = float("inf")
            else:
                best_fit = -float("inf")

            fits = toolbox.map(toolbox.evaluate, offspring)

            for fit, ind in zip(fits, offspring):
                ind.fitness.values = fit
                if minimize:
                    if fit[0] < best_fit:
                        best_ind = ind
                else:
                    if fit[0] > best_fit:
                        best_ind = ind

            # Update the best solution if one coalition of the new generation is better
            hall_of_fame.update(offspring)

            # Replace the old population
            population = offspring

            # Check if the best individual of the new generation improves the solution
            # If not allow n_no_improvement iterations before stopping the algorithm
            if minimize:
                if self.model(best_ind.actions) >= self.model(hall_of_fame.items[0].actions):
                    k_no_improvement += 1
            else:
                if self.model(best_ind.actions) <= self.model(hall_of_fame.items[0].actions):
                    k_no_improvement += 1

            if k_no_improvement == n_no_improvement:
                break

        # Return the best coalition found
        best_coalition = Coalition(hall_of_fame.items[0].actions)

        return best_coalition
        
    def sample_coalitions(self, N_coalitions, available_values=None):
        """
            Draws N random coalitions from the available actions in the model
            1. Pick a random order of appearance of the actions
            2. Pick a random value for each action (possibily restricted by the available_values variable)

            Args:
                N_coalitions (int): the number of samples.
                available_values (dict): a dict containing the lists of available values for each action.

            Returns:
                list: the list of sampled coalitions
        """

        self.prepare_permission_structure(available_values)

        N_actions = len(self.available_actions)
        coalitions = []

        variable_index = {}
        for i in range(len(self.bayesian_network.states)):
            s = self.bayesian_network.states[i]
            variable_index[s.name] = i
        
        for i in range(N_coalitions):
            
            action_order = np.random.choice(np.arange(0, N_actions), N_actions, replace=False).tolist()
            actions = {}

            # For each variable, sample a value based on its probability
            # given the value of the previous variables
            for j in action_order:

                action = self.available_actions[j]
                proba = self.bayesian_network.predict_proba([actions])[0]
                v = list(proba[variable_index[action.name]].parameters[0].keys())
                p = list(proba[variable_index[action.name]].parameters[0].values())
                actions[action.name] = np.random.choice(v, 1, p=p).tolist()[0]

            coalitions.append(Coalition(actions))
            
        return coalitions

    def get_current_coalition(self):
        current_coalition = {}
        for action in self.available_actions:
           current_coalition[action.name] = action.current_value
        return Coalition(current_coalition)

    def get_typical_coalition(self):
        typical_coalition = {}
        for action in self.available_actions:
           typical_coalition[action.name] = action.typical_value
        return Coalition(typical_coalition)

            
    def find_min_max_coalitions(self, coalitions):
        """
            Returns the coalitions with the minimum and the maximum model value in a list of coalitions and their values.

            Args:
                coalitions (list): the list of coalitions.
                values (list): the list of values in which to search for the min and max.

            Returns:
                min_coalition (Coalition): the coalition with the minimum model value.
                max_coalition (Coalition): the coalition with the maximum model value.
        """
        
        max_value = 0
        min_value = 1e12
        
        for coalition in coalitions:
            
            value = coalition["value"]
            del coalition["value"]
            
            if value > max_value:
                max_value = value
                max_coalition = Coalition(coalition)
                        
            if value < min_value:
                min_value = value
                min_coalition = Coalition(coalition)
                
        return min_coalition, max_coalition


    def prepare_permission_structure(self, available_values=None):

        self.independent_actions = [action for action in self.available_actions if len(action.superiors) == 0]
        self.dependent_actions = [action for action in self.available_actions if len(action.superiors) != 0]

        # Create the pomegranate distributions for each action
        # Marginal distributions for actions without superiors
        # Conditional distributions for actions with superiors
        def setup_pog_node(model, action, pog_nodes, pog_dists):

            # If the action has no superior
            # Create the marginal discrete distribution
            # (equal probability affected to each available outcome / value)
            if len(action.superiors) == 0:

                if action.name not in pog_nodes.keys():

                    d = {}
                    if available_values is None:
                        for value in action.values:
                            d[value] = 1.0/len(action.values)
                    else:
                        for value in action.values:
                            if value in available_values[action.name]:
                                d[value] = 1.0/len(available_values[action.name])
                            else:
                                d[value] = 0.0

                    dist = pog.DiscreteDistribution(d)
                    node = pog.Node(dist, name=action.name)
                    pog_dists[action.name] = dist
                    pog_nodes[action.name] = node
                    model.add_states(node)

            # If the action has superiors
            # Create the superiors distributions if needed
            # (and their own superior distributions if needed, recursively)
            # then create the conditional distribution between the action and its superiors
            else:

                superior_nodes = []
                for superior_action in action.superiors:
                    setup_pog_node(model, superior_action, pog_nodes, pog_dists)
                    superior_nodes.append(pog_dists[superior_action.name])

                cpt_df = pd.DataFrame(action.superiors_cpt)
                cpt_df.columns = [a.name for a in action.superiors] + [action.name, "p"]

                if available_values is not None:
                    cpt_df.loc[~cpt_df[action.name].isin(available_values[action.name]), "p"] = 0.0

                cpt_df.set_index([a.name for a in action.superiors] + [action.name], inplace=True)
                cpt_df["p"] = cpt_df["p"]/cpt_df.groupby([a.name for a in action.superiors])["p"].sum()
                cpt_df.reset_index(inplace=True)
                cpt_df.fillna(0, inplace=True)
                cpt = cpt_df.values.tolist()

                dist = pog.ConditionalProbabilityTable(cpt, superior_nodes)
                node = pog.Node(dist, name=action.name)
                pog_dists[action.name] = dist
                pog_nodes[action.name] = node

                model.add_states(node)

                for superior_action in action.superiors:
                    model.add_edge(pog_nodes[superior_action.name], pog_nodes[action.name])

        # Initialize the pomegranate model
        model = pog.BayesianNetwork("bn")
        pog_dists = {}
        pog_nodes = {}

        for action in self.available_actions:
            if action.name not in pog_nodes.keys():
                setup_pog_node(model, action, pog_nodes, pog_dists)

        model.bake()
        
        self.bayesian_network = model


    def prepare_experimentation_plan(self, coalitions, base_coalition):
        """
            Creates a dict of coalitions of actions (one value for each action), based on a list of possible orderings of those actions.
            Removes the duplicate entries to limit the number of model runs.

            Args:
                coalitions (list): a list of possible coalitions.
                base_coaltion (Coalition): the reference coalition.

            Returns:
                experiments (dict): a dict of unique coalitions.
        """

        experiments = []

        # Loop on coalitions
        for coalition in coalitions:
            
            # Start from the base coalition
            coalition_with_action = dict(base_coalition.actions)
            experiments.append(base_coalition.actions)
            
            # Loop on actions in the coalition
            for k, v in coalition.actions.items():
                coalition_with_action[k] = v

                experiments.append(coalition_with_action)

        experiments = pd.DataFrame.from_dict(experiments)
        experiments.drop_duplicates(inplace=True)

        return experiments


    def compute_model_values(self, experiments):

        values = []
        for exp in experiments:
            values.append(self.model(exp))

        return values
    
        
    def compute_shapley_value(self, base_coalition, grand_coalition, N_coalitions):
        """
            Computes the shapley value of each change of action between a reference case (the base coalition) and a final case (the grand coalition).
            1. Sample N_coalitions orderings of actions to get from the base to the grand coalition.
            2. Compute the cumulative marginal effect of each action of these orderings.
            3. Normalize the result to force the shapley values to sum to the difference of model values between the base and grand coalitions
               (which is not the case since we do not consider every possible case, but only a sample of them).

            Args:
                base_coalition (Coalition): the reference coalition of actions .
                grand_coalition (Coalition): the "grand" coalition of actions (all actions have been changed).
                N_coalitions (int): the number of orderings to draw.

            Returns:
                deltas (dict): a dict containing the shapley values of each action change.
        """

        N_actions = len(base_coalition.actions.keys())

        # Sample N randomly ordered grand coalitions
        # (all actions take the value leading to the best coalition)
        available_values = {}
        for k in base_coalition.actions.keys():
            available_values[k] = [grand_coalition.actions[k]]

        coalitions = self.sample_coalitions(N_coalitions, available_values)

        # Prepare the list of intermediary coalitions needed to compute the shapley value
        # (coalitions with some actions set to their "best" values, and the others set to their "base" value)
        intermediary_coalitions = []

        # Set up the permission structure / bayes network
        self.prepare_permission_structure()


        def make_coalition_feasible(coalition_actions, grand_coalition_actions, base_coalition_actions):
            coalition_actions = coalition_actions.copy()

            # Compute the probability of the actions that take their values from the grand coalition
            # based on the values of the actions that take their values from the base coalition
            base_actions = {k: v for k, v in base_coalition.actions.items() if k not in grand_coalition_actions.keys()}
            proba = self.bayesian_network.predict_proba(base_actions)

            for i in range(len(proba)):
                    
                # If the action's value is not already set
                # (= not in the base coalition)
                if type(proba[i]) is not str:
                        
                    action_name = self.bayesian_network.states[i].name
                    p_values = np.array(list(proba[i].parameters[0].values()))
                    k_values = list(proba[i].parameters[0].keys())
                        
                    # If its probability is one, it means the action is determined
                    # by the values of the actions already present in the coalition
                    # and we force its value
                    if np.any(p_values > 1-1e-6):
                        i_max = np.argmax(p_values)
                        coalition_actions.update({action_name: k_values[i_max]})

                    elif p_values[k_values.index(coalition_actions[action_name])] < 1e-6:
                        coalition_actions.update({action_name: base_coalition_actions[action_name]})

            return coalition_actions

        coalition_diffs = {k: [] for k in base_coalition.actions.keys()}

        # For each sampled coalition
        for coalition in coalitions:

            # For each action in this coalition
            for action in base_coalition.actions.keys():

                # Find the index of the action
                action_keys = list(coalition.actions.keys())
                action_index = action_keys.index(action)
                
                # Extract the actions from the coalition
                # up to the index of the action preceding the one being studied
                c1 = list(coalition.actions.items())[0:action_index]
                # up to the index of the action
                c2 = list(coalition.actions.items())[0:(action_index+1)]

                # Create two coalitions from these partial coalitions
                # by overwriting their values in the base coalition
                coalition_without_action = dict(base_coalition.actions)
                coalition_with_action = dict(base_coalition.actions)

                coalition_without_action.update(dict(c1))
                coalition_with_action.update(dict(c2))

                # Force the coalition to be feasible
                coalition_without_action = make_coalition_feasible(coalition_without_action, dict(c1), base_coalition.actions)
                coalition_with_action = make_coalition_feasible(coalition_with_action, dict(c2), base_coalition.actions)

                # If the two coalitions are still different
                if coalition_with_action != coalition_without_action:

                    # Append them to the list of coalitions for which we will need a value
                    intermediary_coalitions += [coalition_without_action, coalition_with_action]

                    # Append them as tuples to compute the marginal value later on
                    coalition_diffs[action].append([tuple(coalition_without_action.values()), tuple(coalition_with_action.values())])

        intermediary_coalitions = pd.DataFrame(intermediary_coalitions)
        intermediary_coalitions.drop_duplicates(inplace=True)
        intermediary_coalitions["value"] = self.compute_model_values(intermediary_coalitions.to_dict("rows"))

        coalition_values_dict = intermediary_coalitions.copy()
        coalition_values_dict.set_index([c for c in coalition_values_dict.columns if c != "value"], inplace=True)
        coalition_values_dict = coalition_values_dict.to_dict()["value"]

        # For each action, compute the marginal effect of its value change
        # (from base value to grand coalition value)
        marginal_values = {}

        for action in base_coalition.actions.keys():
            marginal_values[action] = 0
            for d in coalition_diffs[action]:
                marginal_values[action] += coalition_values_dict[d[1]] - coalition_values_dict[d[0]]

        # Normalize the marginal effects
        exact_total_effect = self.model(grand_coalition.actions) - self.model(base_coalition.actions)
        approx_total_effect = sum(marginal_values.values())
        
        for k, v in marginal_values.items():
            marginal_values[k] *= exact_total_effect/approx_total_effect
            marginal_values[k] = np.round(marginal_values[k], 2)


        ###Repartition of shapley value taking into account permission structure###
        deltas = marginal_values.copy()

        #Ordered available action from bottom to up of hierarchical branches based on permission structure
        ordered_available_action = []
        def create_hierarchical_order(action):
            '''recursive action for ordering in hierarchical order the actions'''
            if len(action.superiors) > 0:
                if action in ordered_available_action:
                    ordered_available_action.remove(action)
                ordered_available_action.append(action)
                for i in range(len(action.superiors)):
                    create_hierarchical_order(action.superiors[i])
            else:  # action don't have parents (top of the pyramid)
                if action in ordered_available_action:
                    ordered_available_action.remove(action)
                ordered_available_action.append(action)

        for action in self.available_actions:
            create_hierarchical_order(action)

        #Update deltas values based on the permission structure and cpt of action associated with the grand coalition
        for action in ordered_available_action:
            if len(action.superiors) > 0:  # action has parents
                el_cpt = action.superiors_cpt
                for i in range(len(action.superiors)):
                    el_cpt = [tup for tup in el_cpt if (tup[i] == grand_coalition.actions[action.superiors[i].name])] #we look for cpt of the grand coalition config to get the correct repartition_key of "responsability" between action and action parents

                el_cpt = [tup for tup in el_cpt if (tup[len(action.superiors)] == grand_coalition.actions[action.name])]

                repartition_key = el_cpt[0][len(el_cpt[0]) - 1] #cpt can be find at the end of the element

                # repartition
                for sup_action in action.superiors:
                    if deltas[sup_action.name] == 0:  # the sup action hasn't change so it has nos responsability in the "couple" overall shapley value. The son action keep all the shapley value.
                        repartition_key_sup_action = 0
                    else:
                        repartition_key_sup_action = repartition_key

                    deltas[sup_action.name] += repartition_key_sup_action * deltas[action.name] / len(action.superiors)
                    deltas[action.name] -= repartition_key_sup_action * deltas[action.name] / len(action.superiors)

            else:
                continue  # we do nothing and the action keep its current shapley value

        return deltas


    def plot_waterfall(self, base_coalition, grand_coalition, shapley_values_to_grand, worst_coalition=None, shapley_values_to_worst=None, interactive=False):
        """
            Plots a waterfall chart showing the effect of each action (shapley value)
            when getting from the base coalition (reference case) to the "grand" coalition (all actions have been changed compared to the reference case).

            Args:
                base_coalition (Coalition): the reference coalition of actions.
                grand_coalition (Coalition): the "grand" coalition of actions (all actions have been changed to their best value).
                shapley_values_to_grand (dict): a dict containing the shapley value of each change of action, between the base and the grand coalition.
                worst_coalition (Coalition): the worst coalition of actions (all actions have been changed to their worst value).
                shapley_values_to_worst (dict): a dict containing the shapley value of each change of action, between the base and the worst coalition.
                interactive (bool): should the plot be drawn in the console (False) or in an interactive plotly web page (True) ?

            Returns:
                None
        """

        # Sort the variables by their marginal impact
        ordered_shapley_values_to_grand = {}
        for k, v in sorted(shapley_values_to_grand.items(), key=lambda item: item[1]):
            if abs(v) > 0.0:
                ordered_shapley_values_to_grand[k] = v

        if worst_coalition is not None:
            ordered_shapley_values_to_worst = {}
            for k, v in sorted(shapley_values_to_worst.items(), key=lambda item: -item[1]):
                if abs(v) > 0.0:
                    ordered_shapley_values_to_worst[k] = -v

        x = []
        y = []
        text = []
        measure = []

        if worst_coalition is not None:
            x.append("Worst coalition")
            y.append(self.model(worst_coalition.actions))
            text.append(self.model(worst_coalition.actions))
            measure.append("relative")

            for k, v in ordered_shapley_values_to_worst.items():
                x.append(k + " - " + worst_coalition.actions[k] + " > " + base_coalition.actions[k])
                y.append(v)
                text.append(str(v))
                measure.append("relative")

        x.append("Base coalition")
        y.append(self.model(base_coalition.actions))
        text.append(self.model(base_coalition.actions))

        if worst_coalition is not None:
            measure.append("total")
        else:
            measure.append("relative")

        for k, v in ordered_shapley_values_to_grand.items():
            x.append(k + " - " + base_coalition.actions[k] + " > " + grand_coalition.actions[k])
            y.append(v)
            text.append(str(v))
            measure.append("relative")

        x.append("Grand coalition")
        text.append(self.model(grand_coalition.actions))
        y.append(self.model(grand_coalition.actions))
        measure.append("total")


        fig = go.Figure(go.Waterfall(
            orientation="h",
            measure=measure,
            x=y,
            textposition="outside",
            text=text,
            y=x,
            connector={"line":{"color":"rgb(63, 63, 63)"}},
            decreasing = {"marker":{"color":"#8BC34A"}},
            increasing = {"marker":{"color":"#F44336"}},
            totals = {"marker":{"color":"#607D8B"}}
        ))

        fig.update_layout(
            margin=dict(l=400, r=50, b=50, t=50, pad=4)
        )

        if interactive:
            plot(fig)
        else:
            fig.show()

class Action():

    def __init__(self, name, values, ordered=False, current_value=None,typical_value=None, type_action=None,forms_list=None):
        """
        Args:
            values (list): possible values for the action
            ordered (bool): are values ordered from worst to best ?
            typical_value : the typical (most common) value among the possible values
            current_value : the current value of the project (unchanged)
            forms_list : a way to link function that can help to get superiors probs for example or for other compute model use
        """

        self.name = name
        self.values = values
        if current_value is None:
            self.current_value = values[0]
        else:
            self.current_value = current_value

        if typical_value is None:
            self.typical_value = values[0]
        else:
            self.typical_value = typical_value

        self.superiors = []
        self.superiors_cpt = None

        self.type_action = type_action
        self.forms_list = forms_list

        # Check that the normal value is in the values
        if typical_value not in values:
            ValueError(
                "The typical_value " + str(current_value) + " is not in the values list (" + ", ".join(values) + ")")

        if current_value not in values:
            ValueError(
                "The current value " + str(current_value) + " is not in the values list (" + ", ".join(values) + ")")


    def add_superiors(self, actions, superiors_cpt):
        self.superiors += actions
        self.superiors_cpt = superiors_cpt


    def add_superiors2(self, actions, possible_combinations):
        self.superiors += actions

        # Prepare a dict listing the values of each superior and of the action
        variables = {}
        for variable in self.superiors:
            variables[variable.name] = variable.values
        variables[self.name] = self.values
        
        # Make a dataframe combining all possible cases
        all_cases_df = cartesian_product(variables)
        all_cases_df.set_index(list(variables.keys()), inplace=True)

        # Make a dataframe with possible cases
        comb_df = pd.DataFrame(possible_combinations)
        comb_df.columns = list(variables.keys())
        comb_df.set_index(list(variables.keys()), inplace=True)
        comb_df["p"] = 1.0

        # Merge the two to build a complete conditional probability table
        cpt_df = pd.merge(all_cases_df, comb_df, on=list(variables.keys()), how="left")
        cpt_df.fillna(0, inplace=True)

        cpt_df["p"] = cpt_df["p"]/cpt_df.groupby([s.name for s in self.superiors])["p"].sum()
        cpt_df.fillna(0, inplace=True)

        cpt_df.reset_index(inplace=True)

        self.superiors_cpt = cpt_df.values.tolist()



class Coalition():

    def __init__(self, actions):
        self.actions = actions
        #TODO : self.value pour ne pas resimuler plein de fois ?

