#shapley is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import sys
import os

script_path = os.path.abspath(__file__)
package_path = os.path.dirname(os.path.dirname(script_path))
sys.path.append(package_path)

from shapley import Shapley, Action, Coalition
import pandas as pd
import numpy as np

# Create the global Shapley object
shapley = Shapley()

# Create a dummy model of carbon footprint
# Input variables are dict of values, with different levels
# Only discrete variables can be used
energy_performance = {
    "low": 30,
    "medium": 20,
    "high": 10
}

emission_factor = {
    "district_heating": 0.1,
    "electricity": 0.2,
    "gas": 0.25
}

temperature_setpoint = {
    "low": 0.9,
    "medium": 1.0,
    "high": 1.1
}

construction_structure = {
    "wood": 600/50,
    "concrete": 700/50
}

district_heating_construction = {
    "district_heating_construction": "district_heating_construction",
    "no_district_heating_construction": "no_district_heating_construction",
}

programmation = {
    "mixed_use": "mixed_use",
    "mono_use": "mono_use"
}

housing_area = {
    "mono_use": 1.0,
    "mixed_use": 0.5
}

office_area = {
    "mono_use": 0.0,
    "mixed_use": 0.5
}

# The model takes a dict of variables key, value pairs
# (one value for each input variable of the model)
# and returns a carbon footprint value
def compute_emissions(coalition):
    
    # Check input
    # if (coalition["district_heating_construction"] == "district_heating_construction" and coalition["energy_performance"] != "high") or \
    #    (coalition["district_heating_construction"] == "no_district_heating_construction" and coalition["energy_performance"] == "high") or \
    #    (coalition["district_heating_construction"] == "district_heating_construction" and coalition["emission_factor"] != "district_heating") or \
    #    (coalition["district_heating_construction"] == "no_district_heating_construction" and coalition["emission_factor"] == "district_heating"):
    #        raise ValueError("Impossible coalition")
    
    # -------
    # Urban print - like
    # Buildings emissions
    build_em = 0
    mob_em = 0
    constr = 0
    en = energy_performance[coalition["energy_performance"]]
    ef = emission_factor[coalition["emission_factor"]]
    ts = temperature_setpoint[coalition["temperature_setpoint"]]
    constr = construction_structure[coalition["construction_structure"]]
    build_em = en*ef*ts + constr
    build_em = build_em*(housing_area[coalition["programmation"]] + office_area[coalition["programmation"]])
    
    # Mobility emissions
    if coalition["programmation"] == "mixed_use":
        mob_em = 0.0
    else:
        mob_em = 10.0
        
    # Total emissions
    em = build_em + mob_em
    
    # ------
    
    return em

# Add the model to the shapley object
shapley.add_value_model(compute_emissions)

# Create actions based on the variables of the model
a1 = Action("energy_performance", values=["low", "medium", "high"], ordered=True, typical_value="medium")
a2 = Action("emission_factor", values=["gas", "electricity", "district_heating"], ordered=False, typical_value="gas")
a3 = Action("temperature_setpoint", values=["high", "medium", "low"], ordered=True, typical_value="medium")
a4 = Action("construction_structure", values=["concrete", "wood"], ordered=True, typical_value="concrete")
a5 = Action("district_heating_construction", values=["district_heating_construction", "no_district_heating_construction"], ordered=False, typical_value="no_district_heating_construction")
a6 = Action("programmation", values=["mixed_use", "mono_use"], ordered=False, typical_value="mono_use")

shapley.add_available_actions([a1, a2, a3, a4, a5, a6])

# Create a permission structure restricting the values that actions can take
# based on the value of other previous actions in the coalition
a2.add_superiors([a5],
     [["district_heating_construction", "district_heating", 1.0],
       ["district_heating_construction", "electricity", 0.0],
       ["district_heating_construction", "gas", 0.0],
       ["no_district_heating_construction", "district_heating", 0.0],
       ["no_district_heating_construction", "electricity", 0.5],
       ["no_district_heating_construction", "gas", 0.5]]
)

a1.add_superiors([a5],
      [["district_heating_construction", "low", 0.0],
        ["district_heating_construction", "medium", 0.0],
        ["district_heating_construction", "high", 1.0],
        ["no_district_heating_construction", "low", 0.5],
        ["no_district_heating_construction", "medium", 0.5],
        ["no_district_heating_construction", "high", 0.0]]
)
        
min_coalition = shapley.find_best_coalition(n_population=30, n_generations=10, n_no_improvement=5, minimize=True)
max_coalition = shapley.find_best_coalition(n_population=30, n_generations=10, n_no_improvement=5, minimize=False)

base_coalition = shapley.get_typical_coalition()

sv_avoidable = shapley.compute_shapley_value(base_coalition, min_coalition, 10)
sv_avoided = shapley.compute_shapley_value(base_coalition, max_coalition, 10)

shapley.plot_waterfall(base_coalition, min_coalition, sv_avoidable, max_coalition, sv_avoided, interactive=True)

# Check convergence
sv_avoidable = []
for i in np.arange(0, 10):
    sv_avoidable.append(shapley.compute_shapley_value(base_coalition, min_coalition, i+1))
    
df = pd.DataFrame(sv_avoidable)
df["n_coalitions"] = np.arange(0, 10)
df = df.melt("n_coalitions")

import plotly.express as px
from plotly.offline import plot
fig = px.line(df, x="n_coalitions", y="value", color="variable")
plot(fig)
